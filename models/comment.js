var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    email: {type: String, required: true},
    productId: {type: String, required: true},
    details: {type: String, required: true},
});


module.exports = mongoose.model('Comment', schema);