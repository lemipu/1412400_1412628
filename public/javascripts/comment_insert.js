$(document).ready(function () {
    // This will fire once the page has been fully loaded.
    $('#comment-post-btn').click(function () {
        comment_post_btn_click();
    });
});

function comment_post_btn_click() {
    // Get text within textarea
    var _comment = $('#comment-post-text').val().replace(/\n/g, "<br />");
    var _userEmail = $('#userEmail').val();
    var _productId = $('#productId').val();

    if (_comment.length > 0 && _userEmail != null) {
        // Proceed with our ajax callback
        $('.comment-insert-container').css('border', '1px solid #e1e1e1');
        console.log(_userEmail);

        $.post("/product-details/comment-insert",
            {
                task: "comment_insert",
                userEmail: _userEmail,
                productId: _productId,
                userComment: _comment
            }
        ).error(
            function () {
                console.log("Error!!!");
            }
        ).success(
            function (data) {
                comment_insert( data);
                console.log("ResponseText: " + JSON.stringify(data));
            }
        );
    } else {
        $('.comment-insert-container').css('border', '1px solid #ff0000');
        console.log("The text area is empty");
    }

    // Clear textarea.
    $('#comment-post-text').val("");
}

function comment_insert(data){
    var t = '';
    t += '<li class="comment-holder" id="'+data._id+'">';
    t += '    <div class="user-img">';
    t += '        <img src="/images/user_default.png" class="user-img-pic"/>';
    t += '    </div>';
    t += '    <div class="comment-body">';
    t += '        <h3 class="panel-title">';
    t += '            '+data.email+'';
    t += '        </h3>';

    t += '        <div class="comment-text">';
    t += '            '+data.details+'';
    t += '       </div>';
    t += '    </div>';

    t += '    <div class="comment-buttons-holder">';
    t += '        <ul>';
    t += '            <li class="delete-btn">X</li>';
    t += '        </ul>';
    t += '    </div>';
    t += '</li>';

    $('.comments-holder-ul').prepend(t);
}