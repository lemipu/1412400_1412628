$(document).ready(function () {
    $('#loadMoreBtn').click(function () {
        comment_loadmore_click();
    });
});

function comment_loadmore_click(){
    var _pageNumber = $('#page-number').val();
    var _productId = $('#productId').val();

    $.post("/product-details/comment-loadmore",
        {
            pageNumber : _pageNumber,
            productId: _productId
        }
    ).error(
        function () {
            console.log("Error!!!");
        }
    ).success(
        function (data, loadMore) {
            comment_insert_more(data.comments, data.loadMore)
        }
    );
}

function comment_insert_more(data, loadMore){
    for (var i = 0; i < data.length; i++){
        var t = '';
        t += '<li class="comment-holder" id="'+data[i]._id+'">';
        t += '    <div class="user-img">';
        t += '        <img src="/images/user_default.png" class="user-img-pic"/>';
        t += '    </div>';
        t += '    <div class="comment-body">';
        t += '        <h3 class="panel-title">';
        t += '            '+data[i].email+'';
        t += '        </h3>';

        t += '        <div class="comment-text">';
        t += '            '+data[i].details+'';
        t += '       </div>';
        t += '    </div>';

        t += '    <div class="comment-buttons-holder">';
        t += '        <ul>';
        t += '            <li class="delete-btn">X</li>';
        t += '        </ul>';
        t += '    </div>';
        t += '</li>';

        $('.comments-holder-ul').append(t);
    }
    if (loadMore == false){
        $('.page-number').html("");
    } else {
        var currentValue = $('#page-number').val();
        document.getElementById('#page-number').value = currentValue+1;
    }
}