var Product = require('../models/product');

exports.product_insert = function (req, res, next) {
    console.log("-----");
    console.log(req.body);
    console.log("-----");
    // model
    var product_instance = new Product({
        imagePath: req.body.item_sku,
        title: req.body.item_title,
        description: req.body.item_size,
        price: req.body.item_price,
    });
    // Save the new model instance, passing a callback
    product_instance.save(function (err) {
        if (err) {
            console.log(err);
            return;
        }
        // saved!
        //pass data to view
        res.render('item', {
            layout: 'main_layout',
            title: 'Add new product',
            activeProduct: true,
            activeAddProduct: true
        });
    });
};

exports.product_list = function (req, res, next) {
    Product.find({}, function (err, products) {
        if (err){
            console.log(err);
            return;
        }

        res.render('products', {
            layout: 'main_layout',
            title: 'All Products',
            activeProduct: true,
            activeAllProducts: true,
            items: products
        });
    });
};