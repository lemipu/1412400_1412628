var express = require('express');
var router = express.Router();
var User = require('../models/user');

/* GET home page. */
router.get('/',isLoggedInAsAdmin, function(req, res, next) {
    User.find(function (err, docs) {
        res.render('user/edit-user', { title: 'Edit Users', users: docs });
    });
});

router.get('/users',isLoggedInAsAdmin, function(req, res){
    var searchName = req.query.search;
    const regexName = new RegExp(escapeRegex(searchName), 'gi');
    User.find({title: regexName}, function(err,docs){
        res.render('user/edit-user', { title: 'Edit Users', users: docs });
    });
});

function escapeRegex(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$!#\s]/g, "\\$&");
}

module.exports = router;

function isLoggedInAsAdmin(req, res, next) {
    if (req.isAuthenticated() && req.user.role === "admin") {
        return next();
    }
    res.redirect('/');
}
