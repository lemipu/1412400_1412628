var express = require('express');
var router = express.Router();
var User = require('../models/user');

router.get('/', function(req, res, next) {
    currentUser = req.user;
    activeUser = currentUser.id;
    User.find(function (err, docs) {
        res.render('user/delete-user', { title: 'Delete Users', users: docs });
    });
});

router.get('/delete/:id', function(req, res, next){
    var userId = req.params.id;
    var activeUser = req.user.id;
    if(userId === activeUser){
        res.redirect('/edit-user');
    } else
    User.findByIdAndRemove(userId, function(err, user){
         if (err){
             return res.redirect('/edit-user');
         }
        res.redirect('/edit-user');
    });
});

module.exports = router;
