var express = require('express');
var router = express.Router();
var Cart = require('../models/cart');

var Product = require('../models/product');
var Order = require('../models/order');

/* GET home page. */
router.get('/', function (req, res, next) {
    var successMsg = req.flash('success')[0];
    currentUser = req.user;
    Product.find(function (err, docs) {
        var productChunks = [];
        var chunkSize = 3;
        for (var i = 0; i < docs.length; i += chunkSize) {
            productChunks.push(docs.slice(i, i + chunkSize));
        }
        res.render('shop/index', {
            title: 'Shopping Cart',
            products: productChunks,
            successMsg: successMsg,
            noMessages: !successMsg,
            user: currentUser
        });
    });
});

router.get('/add-to-cart/:id', function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    Product.findById(productId, function (err, product) {
        if (err) {
            return res.redirect('/');
        }
        cart.add(product, product.id);
        req.session.cart = cart;
        console.log(req.session.cart);
        res.redirect('/');
    });
});

router.get('/reduce/:id', function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    cart.reduceByOne(productId);
    req.session.cart = cart;
    res.redirect('/shopping-cart');
});

router.get('/remove/:id', function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    cart.removeItem(productId);
    req.session.cart = cart;
    res.redirect('/shopping-cart');
});

router.get('/shopping-cart', function (req, res, next) {
    if (!req.session.cart) {
        return res.render('shop/shopping-cart', {products: null});
    }
    var cart = new Cart(req.session.cart);
    res.render('shop/shopping-cart', {title: "Shopping Cart", products: cart.generateArray(), totalPrice: cart.totalPrice});
});

router.get('/checkout', isLoggedIn, function (req, res, next) {
    if (!req.session.cart) {
        return res.redirect('/shopping-cart');
    }
    var cart = new Cart(req.session.cart);
    var errMsg = req.flash('error')[0];
    res.render('shop/checkout', {total: cart.totalPrice, errMsg: errMsg, noError: !errMsg});
});

router.post('/checkout', isLoggedIn, function (req, res, next) {
    if (!req.session.cart) {
        return res.redirect('/shopping-cart');
    }
    var cart = new Cart(req.session.cart);

    var order = new Order({
        user: req.user,
        cart: cart,
        address: req.body.address,
        name: req.body.name,
        phone: req.body.phone
    });
    order.save(function (err, result) {
        req.flash('success', 'Successfully bought product!');
        req.session.cart = null;
        res.redirect('/');

    });
});

// Search
router.get('/games', function (req, res) {
    var successMsg = req.flash('success')[0];
    currentUser = req.user;

    var searchName = req.query.search;
    var searchType = req.query.type;
    if (searchType == "Any") {
        searchType = "";
    }
    var searchYear = req.query.year;
    var searchMinPrice = req.query.priceMin;
    if (searchMinPrice == "") {
        searchMinPrice = 0;
    }
    var searchMaxPrice = req.query.priceMax;
    if (searchMaxPrice == ""){
        searchMaxPrice = 9999;
    }
    const regexName = new RegExp(escapeRegex(searchName), 'gi');
    const regexType = new RegExp(escapeRegex(searchType), 'gi');
    if (searchYear == ""){
        Product.find({title: regexName, price: {$gte: searchMinPrice, $lte: searchMaxPrice}, type: regexType}, function (err, allProducts) {
            if (err) {
                console.log(err);
            } else {
                if (allProducts.length < 1) {
                    var noMatch = "No games match your search."
                    res.render('shop/index', {
                        title: 'Shopping Cart', searchResult: noMatch, successMgs: successMsg, noMessages: !successMsg,
                        user: currentUser
                    });
                } else {
                    var productChunks = [];
                    var chunkSize = 3;
                    for (var i = 0; i < allProducts.length; i += chunkSize) {
                        productChunks.push(allProducts.slice(i, i + chunkSize));
                    }
                    res.render('shop/index', {
                        title: 'Shopping Cart',
                        products: productChunks,
                        successMsg: successMsg,
                        noMessages: !successMsg,
                        user: currentUser
                    });
                }
            }
        });
    } else {
        Product.find({title: regexName, price: {$gte: searchMinPrice, $lte: searchMaxPrice}, type: regexType, releaseYear: searchYear}, function (err, allProducts) {
            if (err) {
                console.log(err);
            } else {
                if (allProducts.length < 1) {
                    var noMatch = "No games match your search."
                    res.render('shop/index', {
                        title: 'Shopping Cart', searchResult: noMatch, successMgs: successMsg, noMessages: !successMsg,
                        user: currentUser
                    });
                } else {
                    var productChunks = [];
                    var chunkSize = 3;
                    for (var i = 0; i < allProducts.length; i += chunkSize) {
                        productChunks.push(allProducts.slice(i, i + chunkSize));
                    }
                    res.render('shop/index', {
                        title: 'Shopping Cart',
                        products: productChunks,
                        successMsg: successMsg,
                        noMessages: !successMsg,
                        user: currentUser
                    });
                }
            }
        });
    }
});

function escapeRegex(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$!#\s]/g, "\\$&");
}

module.exports = router;

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    req.session.oldUrl = req.url;
    res.redirect('/user/signin');
}

function isLoggedInAsAdmin(req, res, next) {
    if (req.isAuthenticated() && req.user.role === "admin") {
        return next();
    }
    res.redirect('/');
}