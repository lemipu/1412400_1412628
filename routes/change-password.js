var express = require('express');
var router = express.Router();
var User = require('../models/user');

/* GET home page. */
router.get('/',isLoggedIn, function(req, res, next) {
    currentUser = req.user;
    User.find(function (err, docs) {
        res.render('user/change-password', { title: 'Edit Users', user: currentUser });
    });
});

router.get('/:id',isLoggedIn, function(req, res, next){
    currentUser = req.user;
    userId = currentUser.id;
    User.findById(userId, function(err, docs){
        if (err)
            res.redirect('/edit-user');
        res.render('user/change-password', {title: 'Edit Users', user: currentUser});
    });
});

router.post('/:id', function(req, res, next){
    currentUser = req.user;
    userId = currentUser.id;

    var newUser = new User();
    if (req.body.newPassword !== req.body.rePassword) {
        return done(null, false, {message: 'Wrong password retyping.'});
    }
    if (!user.validPassword(password)) {
        return done(null, false, {message: 'Wrong password.'});
    }
    newUser.password = req.body.newPassword.encryptPassword(password);
    User.findByIdAndUpdate(userId,{ password: newPassword},function(err, docs){
        console.log(err);
    });

    console.log(newUser.password);
    console.log(password);
    console.log(rePassword);

    res.redirect('/user/profile');
});

module.exports = router;

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

function isLoggedInAsAdmin(req, res, next) {
    if (req.isAuthenticated() && req.user.role === "admin") {
        return next();
    }
    res.redirect('/');
}