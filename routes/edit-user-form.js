var express = require('express');
var router = express.Router();
var User = require('../models/user');

/* GET home page. */
router.get('/',isLoggedIn, function(req, res, next) {
    currentUser = req.user;
    User.find(function (err, docs) {
        res.render('user/edit-user-form', { title: 'Edit Users', user: currentUser });
    });
});

router.get('/:id',isLoggedIn, function(req, res, next){
    userId = req.params.id;
    User.findById(userId, function(err, docs){
        if (err)
            res.redirect('/edit-user');
        res.render('user/edit-user-form', {title: 'Edit Users', user: docs});
    });
});

router.post('/:id', function(req, res, next){
    currentUser = req.user;
    userId = req.params.id;

    newName = req.body.name;
    newPhone = req.body.phone;
    newAddress = req.body.address;
    newRole = req.body.address;
    User.findByIdAndUpdate(userId,{ name: newName, phone: newPhone, address: newAddress, role: newRole},function(err, docs){
        console.log(err);
    });
    if (currentUser.role==='admin') {
        res.redirect('/user/profile');
    }else res.redirect('/');
});

module.exports = router;

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

function isLoggedInAsAdmin(req, res, next) {
    if (req.isAuthenticated() && req.user.role === "admin") {
        return next();
    }
    res.redirect('/');
}