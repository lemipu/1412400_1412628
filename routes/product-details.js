var express = require('express');
var router = express.Router();
var Product = require('../models/product');
var Comment = require('../models/comment');

router.get('/:id', function(req, res, next){
    var currentProductId = req.params.id;
    var currentUser = req.user;
    Product.findById(currentProductId, function(err,docs){
        var _currentProduct = docs;
        var _relatedMessage = true;
        var _currentType = _currentProduct.type;
        // Find product with same type:
        Product.find({_id: { $ne : currentProductId},type : _currentType}, function(err, docs){
            var _relatedProducts = docs;
            if (_relatedProducts.length < 1){
                _relatedMessage = false;
            }
            if (_relatedProducts.length > 4){
                var _relatedProductChunks = [];
                var chunkSize = 4;
                for (var i = 0; i < _relatedProducts.length; i += chunkSize) {
                    _relatedProductChunks.push(_relatedProducts.slice(i, i + chunkSize));
                }
                var _relatedProductsActive = _relatedProductChunks[0];
                var _relatedProductsNonActive = _relatedProducts.slice(4, _relatedProducts.length);
                var _relatedProductsNonActiveChunks = [];
                for (var o = 0; o < _relatedProductsNonActive.length; o+=chunkSize){
                    _relatedProductsNonActiveChunks.push(_relatedProductsNonActive.slice(o,o+chunkSize));
                }
                // Find all comments
                Comment.find( {productId: currentProductId}, function(err,docs){
                    var _comments = docs;
                    _comments.reverse();
                    var _loadMore = false;
                    if (_comments.length > 5){
                        _commentsFirstPage = _comments.slice(0,5);
                        _loadMore = true;
                    }
                    res.render('products/product-details', { title: 'Product Details', user: currentUser, product: _currentProduct,
                        relatedMessage : _relatedMessage,
                        relatedProductsActive: _relatedProductsActive,
                        relatedProductNonActive: _relatedProductsNonActiveChunks, comments: _comments, loadMore: _loadMore , pageNumber : 1});
                });
            } else {
                Comment.find( {productId: currentProductId}, function(err,docs){
                    var _comments = docs;
                    _comments.reverse();
                    var _loadMore = false;
                    if (_comments.length > 5){
                        _comments = _comments.slice(0,5);
                        _loadMore = true;
                    }
                    res.render('products/product-details', { title: 'Product Details', user: currentUser, product: _currentProduct,
                        relatedMessage : _relatedMessage,
                        relatedProductsActive: _relatedProducts, comments: _comments, loadMore: _loadMore, pageNumber : 1});
                });
            }
        });
    });
});

router.post('/comment-insert', function(req, res){
    var data = req.body;

    if (data.task == 'comment_insert'){
        _userEmail = data.userEmail;
        _productId = data.productId;
        _comment = data.userComment;

        var newComment = new Comment();
        newComment.email = _userEmail;
        newComment.productId = _productId;
        newComment.details = _comment;

        newComment.save();

        res.send(newComment);
    }
});

router.post('/comment-loadmore', function(req, res){
    var page = req.body;
    Comment.find( {productId: page.productId}, function(err,docs){
        var _comments = docs;
        _comments.reverse();
        if (_comments.length > page.pageNumber*5){
            if (_comments.length > (page.pageNumber*5)+5) {
                var _moreComments = _comments.slice(page.pageNumber * 5, (page.pageNumber * 5) + 5);
                var _loadMore = true;
                res.send({comments: _moreComments, loadMore: _loadMore});
            } else {
                var _moreComments = _comments.slice(page.pageNumber * 5, _comments.length);
                var _loadMore = false;
                res.send({comments: _moreComments, loadMore: _loadMore});
            }
        }
    });
})

module.exports = router;