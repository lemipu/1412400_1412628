var express = require('express');
var router = express.Router();
var Product = require('../models/product');

router.get('/', function(req, res, next) {
    Product.find(function (err, docs) {
        res.render('products/delete-product', { title: 'Delete Products', products: docs });
    });
});

router.get('/delete/:id', function(req, res, next){
    var productId = req.params.id;
    Product.findByIdAndRemove(productId, function(err, product){
        if (err){
            return res.redirect('/edit-product');
        }
        res.redirect('/edit-product');
    });
});

module.exports = router;
