var express = require('express');
var router = express.Router();
var Product = require('../models/product');

/* GET home page. */
router.get('/',isLoggedInAsAdmin, function(req, res, next) {
    Product.find(function (err, docs) {
        res.render('products/edit-product-form', { title: 'Edit Products', products: docs });
    });
});

router.get('/:id',isLoggedInAsAdmin, function(req, res, next){
    productId = req.params.id;
    Product.findById(productId, function(err, docs){
        if (err)
            res.redirect('/edit-product');
        res.render('products/edit-product-form', {title: 'Edit Products', product: docs});
});
});

router.post('/:id', function(req, res, next){
    var productId = req.params.id;
    newTitle = req.body.title;
    newImagePath = req.body.url;
    newDescription = req.body.description;
    newPrice = req.body.price;
    newType = req.body.type;
    newPublisher = req.body.publisher;
    newDeveloper = req.body.developer;
    newPlatform = req.body.platform;
    newReleaseYear = req.body.releaseYear;
    Product.findByIdAndUpdate(productId,{ title: newTitle, imagePath: newImagePath, description: newDescription,
        price: newPrice,type: newType, publisher: newPublisher, developer: newDeveloper, platform: newPlatform, releaseYear: newReleaseYear},
        function(err, docs){
            console.log(err);
    });
    res.redirect('/edit-product');
});

module.exports = router;

function isLoggedInAsAdmin(req, res, next) {
    if (req.isAuthenticated() && req.user.role === "admin") {
        return next();
    }
    res.redirect('/');
}