var express = require('express');
var router = express.Router();
var Product = require('../models/product');

/* GET home page. */
router.get('/',isLoggedInAsAdmin, function(req, res, next) {
    Product.find(function (err, docs) {
        res.render('products/edit-product', { title: 'Edit Products', products: docs });
    });
});

router.get('/products',isLoggedInAsAdmin, function(req, res){
    var searchName = req.query.search;
    const regexName = new RegExp(escapeRegex(searchName), 'gi');
    Product.find({title: regexName}, function(err,docs){
        res.render('products/edit-product', { title: 'Edit Products', products: docs });
    });
});

function escapeRegex(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$!#\s]/g, "\\$&");
}

module.exports = router;

function isLoggedInAsAdmin(req, res, next) {
    if (req.isAuthenticated() && req.user.role === "admin") {
        return next();
    }
    res.redirect('/');
}
