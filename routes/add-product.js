var express = require('express');
var router = express.Router();
var Product = require('../models/product');


/* GET home page. */
router.get('/', isLoggedInAsAdmin, function(req, res, next) {
    Product.find(function (err, docs) {
        res.render('products/add-product', { title: 'Add Products', products: docs });
    });
});

router.post('/', isLoggedInAsAdmin, function(req, res, next){
    var newTitle = req.body.title;
    var newURL = req.body.url;
    var newDescription = req.body.description;
    var newPrice = req.body.price;
    var newType = req.body.type;
    var newPublisher = req.body.publisher;
    var newDeveloper = req.body.developer;
    var newPlatform = req.body.platform;
    var newReleaseYear = req.body.releaseYear;

    var newProduct = new Product();
    newProduct.imagePath = newURL;
    newProduct.title = newTitle;
    newProduct.description = newDescription;
    newProduct.price = newPrice;
    newProduct.type = newType;
    newProduct.publisher = newPublisher;
    newProduct.developer = newDeveloper;
    newProduct.platform = newPlatform;
    newProduct.releaseYear = newReleaseYear;

    newProduct.save(function (err) {
        if (err)
            res.redirect('/add-product');
        res.redirect('/add-product');
    });
})
module.exports = router;

function isLoggedInAsAdmin(req, res, next) {
    if (req.isAuthenticated() && req.user.role === "admin") {
        return next();
    }
    res.redirect('/');
}
